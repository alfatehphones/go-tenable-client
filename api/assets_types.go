package api

import (
	"time"
)

type TenableAsset interface {
	GetID()            string
	GetName()          string
	GetSources()       []string
	GetFirstSeen()     time.Time
	GetLastSeen()      time.Time
	GetCreated()       time.Time
	GetUpdated()       time.Time
	GetIPV4Addresses() []string
	GetIPV6Addresses() []string
	GetDefaultIPV4()   string
	GetOS()            string
	GetSystemType()    string
}

type TenableAssetVulnExport struct {
	AgentUUID         string   `json:"agent_uuid"`
	BIOSUUID          string   `json:"bios_uuid"`
	DeviceType        string   `json:"device_type"`
	FQDN              string   `json:"fqdn"`
	Hostname          string   `json:"hostname"`
	UUID              string   `json:"uuid"`
	IPv6              string   `json:"ipv6"`
	IPv4              string   `json:"ipv4,omitempty"`
	LastAuthResults   string   `json:"last_authenticated_results"`
	LastUnauthResults string   `json:"last_unauthenticated_results"`
	MACAddress        string   `json:"mac_address"`
	NetBIOSName       string   `json:"netbios_name"`
	NetBIOSWorkgroup  string   `json:"netbios_workgroup"`
	OS                []string `json:"operating_system"`
	NetworkID         string   `json:"network_id"`
	Tracked           bool     `json:"tracked"`
}

func (t TenableAssetVulnExport) GetID() string {
	return t.UUID
}

func (t TenableAssetVulnExport) GetName() string {
	return t.Hostname
}

func (t TenableAssetVulnExport) GetSources() []string {
	return []string{}
}

func (t TenableAssetVulnExport) GetFirstSeen() time.Time {
	return time.Time{}
}

func (t TenableAssetVulnExport) GetLastSeen() time.Time {
	return time.Time{}
}

func (t TenableAssetVulnExport) GetCreated() time.Time {
	return time.Time{}
}

func (t TenableAssetVulnExport) GetUpdated() time.Time {
	return time.Time{}
}

func (t TenableAssetVulnExport) GetIPV4Addresses() []string {
	return []string{t.IPv4}
}

func (t TenableAssetVulnExport) GetIPV6Addresses() []string {
	return []string{t.IPv6}
}

func (t TenableAssetVulnExport) GetDefaultIPV4() string {
	return t.IPv4
}

func (t TenableAssetVulnExport) GetOS() string {
	if len(t.OS) > 0 {
		return t.OS[0]
	}
	return "unknown"
}

func (t TenableAssetVulnExport) GetSystemType() string {
	return "unknown"
}

type TenableAssetSearch struct {
	// system identification
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	HostName string   `json:"host_name"`
	Sources  []string `json:"sources,omitempty"`
	// timing
	FirstSeen time.Time `json:"first_observed"`
	LastSeen  time.Time `json:"last_observed"`
	Created   time.Time `json:"created"`
	Updated   time.Time `json:"updated"`
	// networks
	IPv4Addresses []string       `json:"ipv4_addresses"`
	IPv6Addresses []string       `json:"ipv6_addresses"`
	IPv4          string         `json:"display_ipv4_address"`
	Network       TenableNetwork `json:"network"`
	// OS things
	OperatingSystems []string `json:"operating_systems"`
	OS               string   `json:"display_operating_system"`
	SystemType       string   `json:"system_type"`
	// GCP things
	GCPZone string `json:"gcp_zone,omitempty"`
}

func (t TenableAssetSearch) GetID() string {
	return t.ID
}

func (t TenableAssetSearch) GetName() string {
	return t.Name
}

func (t TenableAssetSearch) GetSources() []string {
	return t.Sources
}

func (t TenableAssetSearch) GetFirstSeen() time.Time {
	return t.FirstSeen
}

func (t TenableAssetSearch) GetLastSeen() time.Time {
	return t.LastSeen
}

func (t TenableAssetSearch) GetCreated() time.Time {
	return t.Created
}

func (t TenableAssetSearch) GetUpdated() time.Time {
	return t.Updated
}

func (t TenableAssetSearch) GetIPV4Addresses() []string {
	return t.IPv4Addresses
}

func (t TenableAssetSearch) GetIPV6Addresses() []string {
	return t.IPv6Addresses
}

func (t TenableAssetSearch) GetDefaultIPV4() string {
	return t.IPv4
}

func (t TenableAssetSearch) GetOS() string {
	return t.OS
}

func (t TenableAssetSearch) GetSystemType() string {
	return t.SystemType
}

type TenableAssetList struct {
	// system identification
	ID       string               `json:"id"`
	HasAgent bool                 `json:"has_agent"`
	Agents   []string             `json:"agent_name"`
	Name     string               `json:"name"`
	FQDNs    []string             `json:"fqdn"`
	Sources  []TenableAssetSource `json:"sources,omitempty"`
	// Scoring
	ACRScore      int32               `json:"acr_score"`
	ACRDrivers    []TenableACRDrivers `json:"acr_drivers"`
	ExposureScore int32               `json:"exposure_score"`
	// timing
	FirstSeen     time.Time         `json:"first_observed"`
	LastSeen      time.Time         `json:"last_observed"`
	Created       time.Time         `json:"created"`
	Updated       time.Time         `json:"updated"`
	ScanFrequency []TenableScanFreq `json:"scan_frequency"`
	// networks
	IPv4Addresses []string `json:"ipv4"`
	IPv6Addresses []string `json:"ipv6"`
	MACAddresses  []string `json:"mac_address"`
	// OS things
	OSes       []string `json:"operating_system"`
	NetBIOS    []string `json:"netbios_name"`
	SystemType string   `json:"system_type"`
	// AWS things
	AWSNames []string `json:"aws_ec2_name"`
}

type TenableScanFreq struct {
	IntervalDays int32 `json:"interval"`
	Frequency    int32 `json:"frequency"`
	Licensed     bool  `json:"licensed"`
}

type TenableACRDrivers struct {
	Name  string   `json:"driver_name"`
	Value []string `json:"driver_value"`
}

type TenableAssetSource struct {
	Name      string    `json:"name"`
	FirstSeen time.Time `json:"first_seen"`
	LastSeen  time.Time `json:"last_seen"`
}

func (t TenableAssetList) GetID() string {
	return t.ID
}

func (t TenableAssetList) GetName() string {
	if len(t.FQDNs) > 0 {
		return t.FQDNs[0]
	}
	return "unknown"
}

func (t TenableAssetList) GetSources() []string {
	sources := make([]string, 0)
	for _, source := range t.Sources {
		sources = append(sources, source.Name)
	}
	return sources
}

func (t TenableAssetList) GetFirstSeen() time.Time {
	return t.FirstSeen
}

func (t TenableAssetList) GetLastSeen() time.Time {
	return t.LastSeen
}

func (t TenableAssetList) GetCreated() time.Time {
	return t.Created
}

func (t TenableAssetList) GetUpdated() time.Time {
	return t.Updated
}

func (t TenableAssetList) GetIPV4Addresses() []string {
	return t.IPv4Addresses
}

func (t TenableAssetList) GetIPV6Addresses() []string {
	return t.IPv6Addresses
}

func (t TenableAssetList) GetDefaultIPV4() string {
	return t.IPv4Addresses[0]
}

func (t TenableAssetList) GetOS() string {
	if len(t.OSes) > 0 {
		return t.OSes[0]
	}
	return "unknown"
}

func (t TenableAssetList) GetSystemType() string {
	return t.SystemType
}

// asset export items
type TenableAssetExport struct {
	// system identification
	ID               string               `json:"id"`
	HasAgent         bool                 `json:"has_agent"`
	HasPluginResults bool                 `json:"has_plugin_results"`
	Name             string               `json:"name"`
	FQDNs            []string             `json:"fqdns"`
	Sources          []TenableAssetSource `json:"sources"`
	AgentUUID        string               `json:"agent_uauid"`
	BIOSUUID         string               `json:"bios_uuid"`
	// Scoring
	ACRScore      string `json:"acr_score"`
	ExposureScore string `json:"exposure_score"`
	// timing
	FirstSeen        time.Time `json:"first_seen"`
	LastSeen         time.Time `json:"last_seen"`
	FirstScan        time.Time `json:"first_scan_time"`
	LastScan         time.Time `json:"last_scan_time"`
	LastAuthScan     time.Time `json:"last_authenticated_scan_date"`
	LastLicensedScan time.Time `json:"last_licensed_scan_date"`
	Deleted          time.Time `json:"deleted_at"`
	Created          time.Time `json:"created_at"`
	Updated          time.Time `json:"updated_at"`
	// lifecycle
	DeletedBy      string `json:"delted_by"`
	LastScanID     string `json:"last_scan_id"`
	LastScheduleID string `json:"last_schedule_id"`
	// networks
	IPv4Addresses []string              `json:"ipv4s"`
	IPv6Addresses []string              `json:"ipv6s"`
	MACAddresses  []string              `json:"mac_addresses"`
	NetInterfaces []TenableAssetNetwork `json:"network_interfaces"`
	NetworkName   string                `json:"network_name"`
	NetworkID     string                `json:"network_id"`
	// OS things
	Hostnames       []string `json:"hostnames"`
	OSes            []string `json:"operating_systems"`
	NetBIOS         []string `json:"netbios_names"`
	SystemTypes     []string `json:"system_types"`
	SSHFingerprints []string `json:"ssh_fingerprints"`
	Software        []string `json:"installed_software"`
	// AWS things
	AWSInstanceName  string    `json:"aws_ec2_name"`
	AWSTerminatedAt  time.Time `json:"terminated_at"`
	AWSTerminatedBy  string    `json:"terminated_by"`
	AWSInstanceID    string    `json:"aws_ec2_instance_id"`
	AWSInstanceType  string    `json:"aws_ec2_instance_type"`
	AWSVPCID         string    `json:"aws_vpc_id"`
	AWSImageID       string    `json:"aws_ec2_instance_ami_id"`
	AWSOwnerID       string    `json:"aws_owner_id"`
	AWSAZ            string    `json:"aws_availability_zone"`
	AWSRegion        string    `json:"aws_region"`
	AWSGroupName     string    `json:"aws_ec2_instance_group_name"`
	AWSInstanceState string    `json:"aws_ec2_instance_state_name"`
	// Azure things
	AzureVMID       string `json:"azure_vm_id"`
	AzureResourceID string `json:"azure_resource_id"`
	// GCP things
	GCPProjectID  string `json:"gcp_project_id"`
	GCPInstanceID string `json:"gcp_instance_id"`
	GCPZone       string `json:"gcp_zone"`
}

type TenableAssetNetwork struct {
	Name       string `json:"name"`
	MACAddress string `json:"name"`
	IPv4       string `json:"ipv4"`
	IPv6       string `json:"ipv6"`
	FQDN       string `json:"fqdn"`
}

func (t TenableAssetExport) GetID() string {
	return t.ID
}

func (t TenableAssetExport) GetName() string {
	if len(t.FQDNs) > 0 {
		return t.FQDNs[0]
	}
	return "unknown"
}

func (t TenableAssetExport) GetSources() []string {
	sources := make([]string, 0)
	for _, source := range t.Sources {
		sources = append(sources, source.Name)
	}
	return sources
}

func (t TenableAssetExport) GetFirstSeen() time.Time {
	return t.FirstSeen
}

func (t TenableAssetExport) GetLastSeen() time.Time {
	return t.LastSeen
}

func (t TenableAssetExport) GetCreated() time.Time {
	return t.Created
}

func (t TenableAssetExport) GetUpdated() time.Time {
	return t.Updated
}

func (t TenableAssetExport) GetIPV4Addresses() []string {
	return t.IPv4Addresses
}

func (t TenableAssetExport) GetIPV6Addresses() []string {
	return t.IPv6Addresses
}

func (t TenableAssetExport) GetDefaultIPV4() string {
	return t.IPv4Addresses[0]
}

func (t TenableAssetExport) GetOS() string {
	if len(t.OSes) > 0 {
		return t.OSes[0]
	}
	return "unknown"
}

func (t TenableAssetExport) GetSystemType() string {
	if len(t.SystemTypes) > 0 {
		return t.SystemTypes[0]
	}
	return "unknown"
}

// asset listings
type TenableAssetsList interface {
	GetAssets() []TenableAsset
	AppendAsset(TenableAsset)
}

// asset search top listing
type TenableAssetsSearchTop struct {
	Assets     []TenableAssetSearch `json:"assets"`
	Pagination TenablePagination    `json:"pagination"`
}

func (t TenableAssetsSearchTop) GetAssets() []TenableAsset {
	assets := make([]TenableAsset, 0)
	for _, asset := range t.Assets {
		assets = append(assets, asset)
	}
	return assets
}

func (t *TenableAssetsSearchTop) AppendAsset(asset TenableAsset) {
	if t.Assets == nil {
		t.Assets = make([]TenableAssetSearch, 0)
	}
	t.Assets = append(t.Assets, asset.(TenableAssetSearch))
}

type TenableAssetsListTop struct {
	Assets []TenableAssetList `json:"assets"`
	Total  int32              `json:"total"`
}

func (t TenableAssetsListTop) GetAssets() []TenableAsset {
	assets := make([]TenableAsset, 0)
	for _, asset := range t.Assets {
		assets = append(assets, asset)
	}
	return assets
}

func (t *TenableAssetsListTop) AppendAsset(asset TenableAsset) {
	if t.Assets == nil {
		t.Assets = make([]TenableAssetList, 0)
	}
	t.Assets = append(t.Assets, asset.(TenableAssetList))
}

type TenableAssetsExportTop struct {
	Assets []TenableAssetExport `json:"assets"`
	Total  int32                `json:"total"`
}

func (t TenableAssetsExportTop) GetAssets() []TenableAsset {
	assets := make([]TenableAsset, 0)
	for _, asset := range t.Assets {
		assets = append(assets, asset)
	}
	return assets
}

func (t *TenableAssetsExportTop) AppendAsset(asset TenableAsset) {
	if t.Assets == nil {
		t.Assets = make([]TenableAssetExport, 0)
	}
	t.Assets = append(t.Assets, asset.(TenableAssetExport))
}
