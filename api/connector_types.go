package api

import "time"

type TenableConnectorList struct {
	Connectors []TenableConnector `sjson:"connectors"`
}

type TenableConnector struct {
	Type            string                      `json:"type"`
	HumanType       string                      `json:"human_type"`
	DataType        string                      `json:"data_type"`
	Name            string                      `json:"name"`
	IsFA            bool                        `json:"is_fa,omitempty"`
	Status          string                      `json:"status"`
	StatusMessage   string                      `json:"status_message"`
	Schedule        TenableConnectorSchedule    `json:"schedule"`
	DateCreated     time.Time                   `json:"date_created"`
	DateModified    time.Time                   `json:"date_modified"`
	Id              string                      `json:"id"`
	ContainerUUID   string                      `json:"container_uuid"`
	Expired         bool                        `json:"expired"`
	IncrementalMode bool                        `json:"incremental_mode"`
	LastSyncTime    time.Time                   `json:"last_sync_time"`
	LastRun         time.Time                   `json:"last_run"`
	Params          TenableConnectorParam       `json:"params"`
	NetworkUUID     string                      `json:"network_uuid"`
	LastSeenUpdated string                      `json:"last_seen_updated"`
	SubAccounts     TenableConnectorSubAccounts `json:"sub_accounts"`
}

type TenableConnectorSchedule struct {
	Units string `json:"units"`
	Value int    `json:"value"`
	Empty bool   `json:"empty"`
}

type TenableConnectorParam struct {
	Status        map[string]TenableConnectorParamStatus `json:"status"`
	Service       string                                 `json:"service"`
	ImportConfig  bool                                   `json:"import_config,omitempty"`
	AutoDiscovery bool                                   `json:"auto_discovery,omitempty"`
}

type TenableConnectorParamStatus struct {
	LastEventSeen    time.Time `json:"last_event_seen,omitempty"`
	ReleaseTimeStamp time.Time `json:"release_timestamp"`
	Message          string    `json:"message"`
	State            string    `json:"state"`
}

type TenableConnectorSubAccounts struct {
	AccountID       string   `json:"account_id"`
	Trails          []string `json:"trails"` //Not sure what this struct looks like, but its being deprecated
	ExternalIP      string   `json:"external_id"`
	IncrementalMode bool     `json:"incremental_mode"`
	RoleARN         string   `json:"role_arn"`
}
