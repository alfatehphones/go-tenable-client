package api

import (
	"encoding/json"
)

type TenableSeverityCount struct {
	Count  json.Number `json:"count"`
	Level  json.Number `json:"level"`
	Name   string 	    `json:"name"`
}
