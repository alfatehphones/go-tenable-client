package api

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type TenableAPI struct {
	HTTPClient *http.Client
}

type TenableTransport struct {
	rt http.RoundTripper
	AccessKey string
	SecretKey string
}

type TenablePagination struct {
	Limit int32  `json:"limit"`
	Total int32  `json:"total"`
	Next  string `json:"next"`
}

func (tt TenableTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if tt.AccessKey == "" || tt.SecretKey == "" {
		return nil, errors.New("missing API key when creating Tenable API client")
	}
	if tt.rt == nil {
		tt.rt = http.DefaultTransport
	}
	req = req.Clone(req.Context())
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "go-tenable")
	api_keys := fmt.Sprintf("accessKey=%s; secretKey=%s;", tt.AccessKey, tt.SecretKey)
	req.Header.Set("X-ApiKeys", api_keys)
	return tt.rt.RoundTrip(req)
}

func (t *TenableAPI) Configure(access_key string, secret_key string) {
	transport := TenableTransport{AccessKey: access_key, SecretKey: secret_key}
	t.HTTPClient = &http.Client{Transport: transport}
}

func (t * TenableAPI) Post(url string, payload io.Reader) ([]byte, error) {
	response, err := t.HTTPClient.Post(url, "application/json", payload)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (t * TenableAPI) Get(url string) ([]byte, error) {
	response, err := t.HTTPClient.Get(url)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
