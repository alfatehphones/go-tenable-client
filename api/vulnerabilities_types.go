package api

import (
	"time"
)

type TenableVulnerabilitiesCount struct {
	Total       int32                  `json:"total"`
	Severities  []TenableSeverityCount `json:"severities"`
}

// vuln listings
type TenableVulnList interface {
	GetVulns() []TenableVuln
	AppendVuln(TenableVuln)
}

type TenableVuln interface {
	GetName()        string
	GetDescription() string
	GetSeverity()    string
	GetHost()        string
	GetPort()        int32
	GetStatus()      string
	GetOutput()      string
	GetLastSeen()    time.Time
	GetFirstSeen()   time.Time
	GetLastFixed()   time.Time
}

// cvss3 temporal vectors
type TenableVulnTemporalVector struct {
	Exploitability string `json:"exploitability"`
	RemediationLevel string `json:"remediation_level"`
	ReportConfidence string `json:"report_confidence"`
	Raw string `json:"raw"`
}

// cvss3 vectors
type TenableVulnVector struct {
	AccessVector string `json:"access_vector"`
	AccessComplexity string `json:"access_complexity"`
	Authentication string `json:"authentication"`
	ConfidentialityImpact string `json:"confidentiality_impact"`
	IntegrityImpact string `json:"integrity_impact"`
	AvailabilityImpact string `json:"availability_impact"`
	Raw string `json:"raw"`
}

// VPR Drivers age
type TenableVulnVPRDriversAge struct {
	LowerBound int32 `json:"lower_bound"`
	UpperBound int32 `json:"upper_bound"`
}

// VPR Drivers
type TenableVulnVPRDrivers struct {
	AgeOfVuln TenableVulnVPRDriversAge `json:"age_of_vuln"`
	ExploitMaturity string `json:"exploit_code_maturity"`
	CVSSImpactScorePredicted bool `json:"cvss_impact_score_predicted"`
	CVSS3ImpactScore float32 `json:"cvss3_impact_score"`
	ThreatIntensityLast28 string `json:"threat_intensity_last28"`
	ThreatSourcesLast28 []string `json:"threat_sources_last28"`
	ProductCoverage string `json:"product_coverage"`
}

// VPR
type TenableVulnVPR struct {
	Score float32 `json:"score"`
	Drivers TenableVulnVPRDrivers `json:"drivers"`
	Updated string `json:"updated"`
}

// vuln plugin
type TenableVulnPlugin struct {
	BugtraqID []int32 `json:"bid"`
	CANVASPackage string `json:"canvas_package"`
	ChecksDefaultAccount bool `json:"checks_for_default_account"`
	ChecksMalware bool `json:"checks_for_malware"`
	CPE []string `json:"cpe"`
	CVE []string `json:"cve,omitempty"`
	CVSS3BaseScore float32 `json:"cvss3_base_score,omitempty"`
	CVSS3TemporalScore float32 `json:"cvss3_temporal_score"`
	CVSS3TemporalVector TenableVulnTemporalVector `json:"cvss3_temporal_vector"`
	CVSS3Vector TenableVulnVector `json:"cvss3_vector"`
	CVSSBaseScore float32 `json:"cvss_base_score"`
	CVSSTemporalScore float32 `json:"cvss_temporal_score"`
	CVSSTemporalVector TenableVulnTemporalVector `json:"cvss_temporal_vector"`
	CVSSVector TenableVulnVector `json:"cvss_vector"`
	D3ElliotName string `json:"d2_elliot_name"`
	Description string `json:"description"`
	ExploitAvailable bool `json:"exploit_available"`
	ExploitFrameworkCANVAS bool `json:"exploit_framework_canvas"`
	ExploitFrameworkCore bool `json:"exploit_framework_core"`
	ExploitFrameworkD2Ellior bool `json:"exploit_framework_d2_elliot"`
	ExploitFrameworkExploitHub bool `json:"exploit_framework_exploithub"`
	ExploitFrameworkMetasploit bool `json:"exploit_framework_metasploit"`
	ExploitabilityEase string `json:"exploitability_ease"`
	ExploitedByMalware bool `json:"exploited_by_malware"`
	ExploitedByNessus bool `json:"exploited_by_nessus"`
	ExploitHubSKU string `json:"exploithub_sku"`
	Family string `json:"family"`
	FamilyID int32 `json:"family_id"`
	HasPatch bool `json:"has_patch"`
	ID int32 `json:"id"`
	InTheNews bool `json:"in_the_news"`
	MetasploitName string `json:"metasploit_name"`
	MSBulletine string `json:"ms_bulletin"`
	Name string `json:"name"`
	PatchPublicationDate time.Time `json:patch_publication_date"`
	ModificationDate time.Time `json:modification_date"`
	PublicationDate time.Time `json:publication_date"`
	RiskFactor string `json:"risk_factor"`
	SeeAlso []string `json:"see_also"`
	Solution string `json:"solution"`
	StigSeverity string `json:"stig_severity"`
	Synopsis string `json:"synopsis"`
	UnsupportedByVendor bool `json:"unsupported_by_vendor"`
	USN string `json:"usn"`
	Version string `json:"version"`
	VulnPublicationDate time.Time `json:"vuln_publication_date"`
        VPR TenableVulnVPR `json:"vpr,omitempty"`
}

// tenable vuln port record
type TenableVulnPort struct {
	Port int32 `json:"port"`
	Protocol string `json:"protocol"`
	Service string `json:"service"`
}

// single scan entry for a vuln
type TenableVulnScanEntry struct {
	CompletedAt string `json:"completed_at"`
	ScheduleUUID string `json:"schedule_uuid"`
	StartedAt string `json:"started_at"`
	UUID string `json:"uuid"`
}

// vuln export items
type TenableVulnExport struct {
	Asset TenableAssetVulnExport `json:"asset"`
	Output string `json:"output"`
	Plugin TenableVulnPlugin `json:"plugin"`
	Port TenableVulnPort `json:"port"`
	RecastReason string `json:"recast_reason"`
	RecastRuleUUID string `json:"recast_rule_uuid"`
	Scan TenableVulnScanEntry `json:"scan"`
	Severity string `json:"severity"`
	SeverityID int32 `json:"severity_id"`
	SeverityDefaultID int32 `json:"severity_default_id"`
	SeverityModificationType string `json:"severity_modification_type"`
	FirstFound time.Time `json:"first_found"`
	LastFixed time.Time `json:"last_fixed"`
	LastFound time.Time `json:"last_found"`
	IndexedAt time.Time `json:"indexed_at"`
	State string `json:"state"`
}

func (t TenableVulnExport) GetName() string {
	return t.Plugin.Name
}

func (t TenableVulnExport) GetDescription() string {
	return t.Plugin.Description
}

func (t TenableVulnExport) GetHost() string {
	return t.Asset.Hostname
}

func (t TenableVulnExport) GetPort() int32 {
	return t.Port.Port
}

func (t TenableVulnExport) GetStatus() string {
	return t.State
}

func (t TenableVulnExport) GetOutput() string {
	return t.Output
}

func (t TenableVulnExport) GetSeverity() string {
	return t.Severity
}

func (t TenableVulnExport) GetFirstSeen() time.Time {
	return t.FirstFound
}

func (t TenableVulnExport) GetLastSeen() time.Time {
	return t.LastFound
}

func (t TenableVulnExport) GetLastFixed() time.Time {
	return t.LastFixed
}

// vuln listings
type TenableVulnsList interface {
	GetVulns() []TenableVuln
	AppendVuln(TenableVuln)
}

type TenableVulnsExportTop struct {
	Vulns[]   TenableVulnExport `json:"vulns"`
	Total      int32              `json:"total"`
}

func (t TenableVulnsExportTop) GetVulns() ([]TenableVuln) {
	vulns := make([]TenableVuln, 0)
	for _, vuln := range t.Vulns {
		vulns = append(vulns, vuln)
	}
	return vulns
}

func (t *TenableVulnsExportTop) AppendVuln(vuln TenableVuln) {
	if t.Vulns == nil {
		t.Vulns = make([]TenableVulnExport, 0)
	}
	t.Vulns = append(t.Vulns, vuln.(TenableVulnExport))
}
