package api

type TenableNetwork struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
