package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"
)

// Search assets in inventory based on the properties listed in the properties argument, by the conditions in the filter argument.
// Results are limited to the value of the limit argument.
// For syntax to use when specifying properties and filters, refer to the official Tenable API documentation.
//
// Example:  api.SearchAllAssets(`{"and":[{"value":"*test*","operator":"wc","property":"search.alias.host"}]}`, 100)
//
// https://developer.tenable.com/reference/io-v3-uw-assets-search
func (t *TenableAPI) SearchAllAssets(filter string, limit int32) (TenableAssetsList, error) {

	assets := TenableAssetsSearchTop{}
	var next_page string
	var err error

	more_pages := true

	for more_pages {
		url := "https://cloud.tenable.com/api/v3/assets/search"
		var payload *strings.Reader
		if next_page != "" {
			payload = strings.NewReader(fmt.Sprintf(`{"next":"%s"}`, next_page))
		} else {
			payload = strings.NewReader(fmt.Sprintf(`{"filter":%s,"limit":%d}`, filter, limit))
		}
		post, err := t.Post(url, payload)
		if err != nil {
			return nil, err
		}

		var page_assets TenableAssetsSearchTop
		err = json.Unmarshal(post, &page_assets)
		if err != nil {
			return nil, err
		}

		if page_assets.Pagination.Next != "" {
			next_page = page_assets.Pagination.Next
			more_pages = true
		} else {
			more_pages = false
		}

		for _, asset := range page_assets.Assets {
			assets.AppendAsset(asset)
		}
	}

	return &assets, err
}

// List all assets. Limited to 5000 assets, per Tenable API limitations.
//
// https://developer.tenable.com/reference/io-v3-uw-assets-search
func (t *TenableAPI) ListAllAssets() (TenableAssetsList, error) {

	var assets TenableAssetsListTop
	var err error

	url := "https://cloud.tenable.com/assets"
	get, err := t.Get(url)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(get, &assets)
	if err != nil {
		return nil, err
	}

	return &assets, err
}

// Export all assets. Monitors export state and returns once all export chunks are ready and parsed.
//
// https://developer.tenable.com/reference/exports-assets-export-status
func (t *TenableAPI) ExportAllAssets(updated_after *int64) (TenableAssetsList, error) {

	asset_list := TenableAssetsExportTop{}
	var export_job TenableExportJob

	// get export ID
	export_url := "https://cloud.tenable.com/assets/export"
	var payload *strings.Reader
	if updated_after == nil {
		payload = strings.NewReader(`{"chunk_size":1000}`)
	} else {
		payload = strings.NewReader(fmt.Sprintf(`{"chunk_size":1000,"filters":{"updated_at":%d}}`, *updated_after))
	}
	export_post, err := t.Post(export_url, payload)
	if err != nil {
		return nil, err
	}

	// monitor export ID until finished
	err = json.Unmarshal(export_post, &export_job)
	if err != nil {
		return nil, err
	}

	// a small sleep before checking status
	time.Sleep(time.Second)

	// download each chunk
	status_url := fmt.Sprintf("https://cloud.tenable.com/assets/export/%s/status", export_job.UUID)

	var waiting_for_export = true
	var export_status TenableExportStatus
	retries := 10

	for waiting_for_export {

		if retries == 0 {
			return nil, errors.New("exceeded maximum error retries when checking export status")
		}

		status_get, err := t.Get(status_url)
		if err != nil {
			retries--
			time.Sleep(time.Second)
			continue
		}

		err = json.Unmarshal(status_get, &export_status)
		if err != nil {
			retries--
			time.Sleep(time.Second)
			continue
		}

		if export_status.Status != "PROCESSING" {
			// job finished or errored, move on
			break
		}

		time.Sleep(5 * time.Second)
	}

	// fetch, then parse chunk and append
	for _, chunk := range export_status.Available {

		// fetch chunk
		var chunk_list = make([]TenableAssetExport, 0)
		chunk_url := fmt.Sprintf("https://cloud.tenable.com/assets/export/%s/chunks/%d", export_job.UUID, chunk)
		chunk_data, err := t.Get(chunk_url)
		if err != nil {
			return nil, err
		}

		// unmarshal
		err = json.Unmarshal(chunk_data, &chunk_list)
		if err != nil {
			return nil, err
		}

		// append assets
		for _, asset := range chunk_list {
			asset_list.AppendAsset(asset)
			asset_list.Total++
		}
	}

	// optionally enrich data?
	return &asset_list, nil
}
