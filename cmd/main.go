package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/go-tenable-client/api"
)

func main() {
	access_key := os.Getenv("TENABLE_API_KEY")
	secret_key := os.Getenv("TENABLE_SECRET_KEY")
	if access_key != "" && secret_key != "" {
		api := api.TenableAPI{}
		api.Configure(access_key, secret_key)

		vulns, err := api.ExportAllVulns()
		if err != nil {
			fmt.Printf("failed to export all vulns: %v\n", err)
		} else {
			vuln_list := vulns.GetVulns()
			fmt.Printf("exportallvulns: %d items, first item: %v\n", len(vuln_list), vuln_list[0])
		}

		assets, err := api.ExportAllAssets(nil)
		if err != nil {
			fmt.Printf("failed to export all assets: %v\n", err)
		} else {
			asset_list := assets.GetAssets()
			if len(asset_list) > 0 {
				fmt.Printf("exportallassets: %d items, first item: %v\n", len(asset_list), asset_list[0])
			} else {
				fmt.Printf("exportallassets returned no results")
			}
		}

		month_ago_timestamp := time.Now().AddDate(0, -1, 0).Unix()
		changed_assets, err := api.ExportAllAssets(&month_ago_timestamp)
		if err != nil {
			fmt.Printf("failed to export changed assets: %v\n", err)
		} else {
			asset_list := changed_assets.GetAssets()
			if len(asset_list) > 0 {
				fmt.Printf("exportallassets changed in last month: %d items, first item: %v\n", len(asset_list), asset_list[0])
			} else {
				fmt.Printf("exportallassets changed in last month returned no results")
			}
		}

		assets, err = api.SearchAllAssets(`{"and":[{"value":"*test*","operator":"wc","property":"search.alias.host"}]}`, 100)
		if err != nil {
			fmt.Printf("failed to get assets: %v\n", err)
		} else {
			asset_list := assets.GetAssets()
			if len(asset_list) > 0 {
				fmt.Printf("searchallassets: %d items, first item: %v\n", len(asset_list), asset_list[0])
			} else {
				fmt.Printf("searchallassets returned no results")
			}
		}

		assets, err = api.ListAllAssets()
		if err != nil {
			fmt.Printf("failed to get assets: %v\n", err)
		} else {
			asset_list := assets.GetAssets()
			if len(asset_list) > 0 {
				fmt.Printf("listallassets: %d items, first item: %v\n", len(asset_list), asset_list[0])
			} else {
				fmt.Printf("listallassets returned no results")
			}
		}

		connectorList, err := api.GetConnectorsList(1000, 0, "")
		if err != nil {
			fmt.Printf("Failed to get list of connectors")
		} else {

			fmt.Printf("GetConnectorList: %+v\n", connectorList)

		}

	} else {
		println("no API key provided, set TENABLE_API_KEY")
	}
}
